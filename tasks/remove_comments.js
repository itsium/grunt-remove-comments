/**
 * grunt-remove-comments
 *
 *
 * Copyright (c) 2014 heimi
 * Licensed under the MIT license.
 */

 'use strict';

 module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  /*
  This function is loosely based on the one found here:
  http://www.weanswer.it/blog/optimize-css-javascript-remove-comments-php/
  */
  function removeComments(str) {
    str = ('__' + str + '__').split('');
    var mode = {
      singleQuote: false,
      doubleQuote: false,
      regex: false,
      blockComment: false,
      lineComment: false,
      condComp: false
    };
    for (var i = 0, l = str.length; i < l; i++) {

      if (mode.regex) {
        if (str[i] === '/' && str[i-1] !== '\\') {
          mode.regex = false;
        }
        continue;
      }

      if (mode.singleQuote) {
        if (str[i] === "'" && str[i-1] !== '\\') {
          mode.singleQuote = false;
        }
        continue;
      }

      if (mode.doubleQuote) {
        if (str[i] === '"' && str[i-1] !== '\\') {
          mode.doubleQuote = false;
        }
        continue;
      }

      if (mode.blockComment) {
        if (str[i] === '*' && str[i+1] === '/') {
          str[i+1] = '';
          mode.blockComment = false;
        }
        str[i] = '';
        continue;
      }

      if (mode.lineComment) {
        if (str[i+1] === '\n' || str[i+1] === '\r') {
          mode.lineComment = false;
        }
        str[i] = '';
        continue;
      }

      if (mode.condComp) {
        if (str[i-2] === '@' && str[i-1] === '*' && str[i] === '/') {
          mode.condComp = false;
        }
        continue;
      }

      mode.doubleQuote = str[i] === '"';
      mode.singleQuote = str[i] === "'";

      if (str[i] === '/') {

        if (str[i+1] === '*' && str[i+2] === '@') {
          mode.condComp = true;
          continue;
        }
        if (str[i+1] === '*') {
          str[i] = '';
          mode.blockComment = true;
          continue;
        }
        if (str[i+1] === '/') {
          str[i] = '';
          mode.lineComment = true;
          continue;
        }
        mode.regex = true;

      }

    }
    return str.join('').slice(2, -2);
  }

  grunt.registerMultiTask('remove_comments', 'Remove comments from given files.', function() {

    var mulitlineComment = /(\/\*([\s\S]*?)\*\/)/gm,
      singleLineComment = /(\/\/(.*)$)/gm,
      stripSpaces = /(\n{2,})/gm,
      options = this.options({
        singleline: true,
        multiline: true,
        stripspaces: false
      });

    this.files[0].src.forEach(function (file) {

      var contents = grunt.file.read(file);

      contents = removeComments(contents);

      if (options.stripspaces) {
        contents = contents.replace(stripSpaces, '\n');
      }

      grunt.file.write(file, contents);

    });

  });

};
